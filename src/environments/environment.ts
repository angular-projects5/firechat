// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBZ43bOOO4khJ2uRQf8EJMeq3bHTcEh2nQ',
    authDomain: 'fire-chat-8134a.firebaseapp.com',
    databaseURL: 'https://fire-chat-8134a.firebaseio.com',
    projectId: 'fire-chat-8134a',
    storageBucket: 'fire-chat-8134a.appspot.com',
    messagingSenderId: '167735778754',
    appId: '1:167735778754:web:556ca7f6493b285bf84a5b'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
