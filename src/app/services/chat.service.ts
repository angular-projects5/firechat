import { Injectable, ChangeDetectorRef } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Mensaje } from '../interface/mensaje.interface';
import { map } from 'rxjs/operators';

import * as firebase from 'firebase/app';
import 'firebase/auth';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private itemsCollection: AngularFirestoreCollection<Mensaje>;
  public chats: Mensaje[] = [];
  public usuario: any = {};

  private userOutput = new Subject();

  constructor(private afs: AngularFirestore) {
    firebase.auth().onAuthStateChanged((user) => {
      console.log(user);
      if (!user) {
        this.userOutput.next(user);
        return;
      }

      this.usuario.nombre = user.displayName;
      this.usuario.uid = user.uid;
      console.log('usuario', this.usuario.uid);
      this.userOutput.next(this.usuario);

    });
  }

  public getUser(): Observable<any> {
    return this.userOutput;
  }

  login(redSocial: string) {
    firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }
  logout() {
    this.usuario = {};
    firebase.auth().signOut();
  }

  cargarMensajes() {
    this.itemsCollection = this.afs.collection<Mensaje>('chats', ref => ref.orderBy('fecha', 'desc').limit(5));
    return this.itemsCollection.valueChanges()
      .pipe(
        map((mensajes: Mensaje[]) => {
          this.chats = [];

          mensajes.map(m => this.chats.unshift(m));

          return this.chats;
        }));
  }

  agregarMensaje(texto: string) {
    const mensaje: Mensaje = {
      nombre: 'Demo',
      mensaje: texto,
      fecha: new Date().getTime(),
      uid: this.usuario.uid
    };

    return this.itemsCollection.add(mensaje);
  }
}
