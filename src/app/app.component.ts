import { Component, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { ChatService } from './services/chat.service';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public chats: Observable<any[]>;
  public tieneUsuarioLogado = false;
  constructor(public chatService: ChatService, private cd: ChangeDetectorRef) {
    this.chatService.getUser()
      .subscribe((user) => {
        this.tieneUsuarioLogado = !!user;
        console.log('usuario logado', this.tieneUsuarioLogado);

        cd.detectChanges();
      });

  }
}
